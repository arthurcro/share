//
//  CreatePostVC.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 27/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase


class CreatePostVC: UIViewController {

    //Outlets
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var titleTxt: InsetTextField!
    @IBOutlet weak var profileImage: RoundedImageView!
    @IBOutlet weak var descriptionTxtView: UITextView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //Variables
    
    var textViewPlaceHolder = UILabel()
    var locationManager : CLLocationManager!
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true
        descriptionTxtView.delegate = self
        addSwipeRightGesture()
        addTapGesture()
        addPlaceHolder()
   
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DataService.instance.getProfileImage(forUser: (Auth.auth().currentUser?.uid)!) { (returnedImage) in
            if let image = returnedImage {
                self.profileImage.image = image
            }
        }
        currentLocation()
    }
    
    
    @IBAction func postButtonTapped(_ sender: Any) {
        if CLLocationManager.locationServicesEnabled() {
            if titleTxt.text != "" && descriptionTxtView.text != "" {
                spinner.isHidden = false
                spinner.startAnimating()
                DataService.instance.postFeed(forUser: (Auth.auth().currentUser?.uid)!, withContent: descriptionTxtView.text, andTitle: titleTxt.text!, atLatitude: latitude, andAtLongitude: longitude, completion: { (success) in
                    if success {
                        self.spinner.stopAnimating()
                        self.spinner.isHidden = true
                        self.dismissFromLeft()
                    }else{
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                    }
                })
            }else{
                
                
            }
        }else{
            presentError(LocationError.noPermission)
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismissFromLeft()
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

//MARK : - UIGesture

extension CreatePostVC : UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    
    func addSwipeRightGesture(){
        let swipe  = UISwipeGestureRecognizer(target: self, action: #selector(closeButtonTapped(_:)))
        view.addGestureRecognizer(swipe)
    }
    func addTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
}

//MARK : - CLLocation
extension CreatePostVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation : CLLocation = locations[0] as CLLocation
        latitude =  userLocation.coordinate.latitude
        longitude = userLocation.coordinate.longitude
    }
    func currentLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
}

//MARK : - UITextViewDelegate
extension CreatePostVC : UITextViewDelegate {
    func addPlaceHolder(){
        let placeholderX : CGFloat = 5
        let placeholderY : CGFloat = 0
        let placeholderWidth = descriptionTxtView.frame.size.width - 5
        let placeholderHeight = descriptionTxtView.frame.size.height
        textViewPlaceHolder.frame = CGRect(x: placeholderX, y: placeholderY, width: placeholderWidth, height: placeholderHeight)
        textViewPlaceHolder.text = "Description..."
        textViewPlaceHolder.font = UIFont(name: "Avenir", size: 16)
        textViewPlaceHolder.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        textViewPlaceHolder.textAlignment = .left
        descriptionTxtView.addSubview(textViewPlaceHolder)
        
    }
    func textViewDidChange(_ textView: UITextView) {
        if descriptionTxtView.text.count > 0 {
            textViewPlaceHolder.isHidden = true
        }else{
            textViewPlaceHolder.isHidden = false
        }
        self.descriptionTxtView.frame = CGRect(x: textView.frame.minX, y: textView.frame.minY, width: textView.frame.width, height: textView.contentSize.height + 30)
    }
}
