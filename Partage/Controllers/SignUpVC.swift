//
//  SignUpVC.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 24/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit
import Firebase

class SignUpVC: UIViewController {

    //Outlets
    @IBOutlet weak var passwordTxt: InsetTextField!
    @IBOutlet weak var loginFieldsStackView: UIStackView!
    @IBOutlet weak var signUpButton: roundedButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailTxt: InsetTextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var signUpButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //Variables
    var userImage : UIImage?
    
    override func viewDidLoad(){
        
        super.viewDidLoad()
        spinner.isHidden = true
        profileImage.isUserInteractionEnabled = true
        
        //Gesture recognizers
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissViewController))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        let imagePickerGesture = UITapGestureRecognizer(target: self, action: #selector(pickUserImage))
        
        swipeGesture.direction = .down
        swipeGesture.delegate = self
        tapGesture.delegate = self
        
        //add Gestures
        view.addGestureRecognizer(swipeGesture)
        view.addGestureRecognizer(tapGesture)
        profileImage.addGestureRecognizer(imagePickerGesture)
        
        //Bind to keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillChange(_ notification : NSNotification){
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let endFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        UIView.animate(withDuration: duration) {
            self.signUpButtonBottomConstraint.constant = endFrame.height + 60
        }
        
    }
    
    @objc func pickUserImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
        signUpButtonBottomConstraint.constant = 60
    }
    @objc func dismissViewController(){
        dismiss(animated: true, completion: nil)
    }
    @IBAction func signUpButtonTapped(_ sender: Any) {
        if emailTxt.text != "" && passwordTxt.text != "" && passwordTxt.text == confirmPasswordTxt.text && userImage != nil {
            spinner.isHidden = false
            spinner.startAnimating()
            AuthService.instance.registerUser(forEmail: self.emailTxt.text!, withPassword: passwordTxt.text!, andProfileImage: userImage, completion: { (success, error) in
                if success {
                    AuthService.instance.logInUser(forEmail: self.emailTxt.text!, withPassword: self.passwordTxt.text!, completion: { (success, error) in
                        if success {
                                self.spinner.isHidden = false
                                self.spinner.startAnimating()
                            self.present((self.storyboard?.instantiateViewController(withIdentifier: "FeedVC"))!, animated: true, completion: nil)
                        }
                    })
                }else{
                    self.spinner.isHidden = true
                    self.spinner.stopAnimating()
                    self.presentError(AuthError.invalidSignUp)
                }
            })
        }else{
            presentError(AuthError.invalidSignUp)
        }
    }
    
}

extension SignUpVC : UIGestureRecognizerDelegate {
    
}

extension SignUpVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage : UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImage = editedImage
        }else if let original = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = original
        }
        if let selectedImageUser = selectedImage {
            //Upload it to firebase
            profileImage.image = selectedImageUser
            self.userImage = selectedImageUser
        }
        self.dismiss(animated: true, completion: nil)
    }
}
