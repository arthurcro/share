//
//  FeedVC.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 24/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit
import Firebase

class FeedVC: UIViewController {

    //Outlets
    @IBOutlet weak var feedTableView: UITableView!
    
    //Variables
    var posts = [FeedPost]()
    var refreshControl = UIRefreshControl()
    //Functions
    override func viewDidLoad() {
        if #available(iOS 10.0, *) {
            feedTableView.refreshControl = refreshControl
        } else {
            feedTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        feedTableView.delegate = self
        feedTableView.dataSource = self
        addSwipeLeftGesture()
        addSwipeRightGesture()
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        feedTableView.estimatedRowHeight = 150
        feedTableView.rowHeight = UITableViewAutomaticDimension
        feedTableView.setContentOffset(CGPoint(x:0, y:self.feedTableView.contentOffset.y - (self.refreshControl.frame.size.height)), animated: true)
        refreshControl.beginRefreshing()
        refreshData()
    }

    @objc func refreshData(){

        DataService.instance.getPostFromFeed { (returnedFeed) in
            self.posts = returnedFeed
            self.posts = self.posts.reversed()
            self.feedTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    func addSwipeLeftGesture(){
        let swipeLeft =  UISwipeGestureRecognizer(target: self, action: #selector(showCreatePostVC))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
    }
    func addSwipeRightGesture(){
        let swipeRight =  UISwipeGestureRecognizer(target: self, action: #selector(showProfileVC))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
    }
    
    @objc func showProfileVC(){
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "ProfileVC")
        presentFromLeft(profileVC!)
    }
    @objc func showCreatePostVC(){
        let createPostVC = storyboard?.instantiateViewController(withIdentifier: "CreatePostVC")
        presentFromRight(createPostVC!)
    }
    @objc func showChat(sender : UITapGestureRecognizer){
        //Retrieve the tapped cell
        let tapLocation = sender.location(in: self.feedTableView)
        let indexPath : NSIndexPath = self.feedTableView.indexPathForRow(at: tapLocation)! as NSIndexPath
        let post = self.posts[indexPath.row]
        //Add create conv
        DataService.instance.createNewConversation(forPost: post.title, withUser1: post.senderId, andUser2: (Auth.auth().currentUser?.uid)!) { (success, key) in
            if success {
                let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
                chatVC?.setUpView(forToID: post.senderId, withConvID: key)
                self.presentFromRight(chatVC!)
            }else{
                self.presentError(MessageError.convDidNotCreate)
            }
        }
    }

}

//MARK : - Table View methods
extension FeedVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell") as? FeedCell else { return UITableViewCell()}
        cell.configureCell(forUser: posts[indexPath.row].senderId, withTitle: posts[indexPath.row].title, andDescription: posts[indexPath.row].description , postedAt: posts[indexPath.row].postedAt)
        //Add gesture recognizer when type on cell != image
        let tap = UITapGestureRecognizer(target: self, action: #selector(showChat(sender:)))
        cell.addGestureRecognizer(tap)
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
}

//MARK : - UIGesture
extension FeedVC : UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    
}

