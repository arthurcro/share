//
//  ChatVC.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 30/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit
import Firebase

class ChatVC: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var placeholderTxt: UILabel!
    @IBOutlet weak var viewSendConstraint: NSLayoutConstraint!
    
    var messages : [Message] = [Message]()
    var toUid : String = ""
    var convID : String = ""
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        if #available(iOS 10.0, *) {
            chatTableView.refreshControl = refreshControl
        } else {
            chatTableView.addSubview(refreshControl)
        }
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        chatTableView.delegate = self
        chatTableView.dataSource = self
        textView.delegate = self
        addSwipeGesture()
        addTapGesture()
        refreshControl.beginRefreshing()
        DataService.instance.REF_CONV.observe(.value) { (datasnapshot) in
            DataService.instance.getAllMessages(forConv: self.convID) { (messages) in
                self.messages = messages
                self.chatTableView.reloadData()
                self.refreshControl.endRefreshing()
                if self.messages.count > 0 {
                    self.chatTableView.scrollToRow(at: IndexPath.init(row: self.messages.count - 1, section: 0), at: .none, animated: false)
                }
            }
        }
        //bind to keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        chatTableView.estimatedRowHeight = 150
        chatTableView.rowHeight = UITableViewAutomaticDimension
        chatTableView.setContentOffset(CGPoint(x:0, y:self.chatTableView.contentOffset.y - (self.refreshControl.frame.size.height)), animated: true)
        refreshData()
    }
    
    @objc func refreshData(){
        DataService.instance.getAllMessages(forConv: self.convID) { (messages) in
            self.messages = messages
            self.chatTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    @objc func keyboardWillChange(_ notification : NSNotification){
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let endFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        UIView.animate(withDuration: duration) {
            self.viewSendConstraint.constant = endFrame.height
        }
    }
    
    func setUpView(forToID toID : String,  withConvID convID: String){
        toUid = toID
        self.convID = convID
        DataService.instance.getProfileImage(forUser: toID) { (returnedImage) in
            self.userImage.image = returnedImage
        }
        DataService.instance.getEmail(fromSenderId: toID) { (returnedEmail) in
            self.userEmail.text = returnedEmail
        }
    }
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismissFromLeft()
    }
    @IBAction func sendButtonTapped(_ sender: Any) {
        if textView.text != "" {
            DataService.instance.uploadMessage(toConv: convID, fromUser: (Auth.auth().currentUser?.uid)!, toUser: toUid, withContent: textView.text, atTime: "idk", completion: { (success) in
                if success {
                    
                }else{
                    //Handle error
                }
            })
        }
    }
    
    
}

//MARK :- Gestures
extension ChatVC : UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    func addSwipeGesture(){
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(closeButtonTapped(_:)))
        swipe.direction = .right
        view.addGestureRecognizer(swipe)
    }
    
    func addTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
}


//MARK :- UITableView
extension ChatVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let isFromMe = (Auth.auth().currentUser?.uid)! == messages[indexPath.row].fromID
        if !isFromMe {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChatOtherUserCell") as? ChatCell else {return UITableViewCell()}
                cell.configureCell(forMessage: messages[indexPath.row])
                return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMeCell") as? ChatCell else {return UITableViewCell()}
            cell.configureCell(forMessage: messages[indexPath.row])
            return cell
        }
    }
    
}


//MARK : - UITextViewDelegate
extension ChatVC : UITextViewDelegate {
    //Add placeholder
    func addPlaceholder(){
        let placeholderX : CGFloat = 5
        let placeholderY : CGFloat = 0
        let placeholderWidth = textView.frame.size.width - 5
        let placeholderHeight = textView.frame.size.height
        placeholderTxt.frame = CGRect(x: placeholderX, y: placeholderY, width: placeholderWidth, height: placeholderHeight)
        placeholderTxt.text = "Send a message..."
        placeholderTxt.font = UIFont(name: "Avenir", size: 18)
        placeholderTxt.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        placeholderTxt.textAlignment = .left
        textView.addSubview(placeholderTxt)
    }
    //Show or hide the textView when typing
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 0 {
            placeholderTxt.isHidden = true
        }else{
            placeholderTxt.isHidden = false
        }
        self.textView.frame = CGRect(x: textView.frame.minX, y: textView.frame.minY, width: textView.frame.width, height: textView.contentSize.height + 30)
    }
}


