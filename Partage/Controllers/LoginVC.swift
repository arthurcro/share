//
//  ViewController.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 23/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true
        addTapGesture()
        addSwipeUpGesture()
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        if emailTxt.text != "" && passwordTxt.text != "" {
            spinner.isHidden = false
            spinner.startAnimating()
            AuthService.instance.logInUser(forEmail: emailTxt.text!, withPassword: passwordTxt.text!, completion: { (success, error) in
                if success {
                    self.spinner.isHidden = true
                    self.spinner.stopAnimating()
                    let feedVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedVC")
                    self.present(feedVC!, animated: true, completion: nil)
                }else{
                    self.spinner.isHidden = true
                    self.spinner.stopAnimating()
                    self.presentError(AuthError.invalidLogin)
                }
            })
        }
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        let signUpVc = storyboard?.instantiateViewController(withIdentifier: "SignUpVC")
        present(signUpVc!, animated: true, completion: nil)
    }
    //TODO : Reset password
    @IBAction func forgetPasswordButtonTapped(_ sender: Any) {

    }
    
    
    
}

//MARK : - Gestures
extension LoginVC : UIGestureRecognizerDelegate {
    
    func addTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
    }
    func addSwipeUpGesture(){
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(signUpButtonTapped(_:)))
        swipeUp.direction = .up
        view.addGestureRecognizer(swipeUp)
    }
}
