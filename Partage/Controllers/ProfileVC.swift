//
//  ProfileVC.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 27/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit
import Firebase

class ProfileVC: UIViewController {

    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var profileImage: RoundedImageView!
    @IBOutlet weak var convTableView: UITableView!
    //Variables
    var userImage : UIImage?
    var conversations : [Dictionary<String, String>] =  [Dictionary<String, String>]()
    var refreshControl = UIRefreshControl()
    //Functions
    override func viewDidLoad() {
        if #available(iOS 10.0, *) {
            convTableView.refreshControl = refreshControl
        } else {
            convTableView.addSubview(refreshControl)
        }
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        convTableView.delegate = self
        convTableView.dataSource = self
        profileImage.isUserInteractionEnabled = true
        let imagePickerGesture = UITapGestureRecognizer(target: self, action: #selector(pickUserImage))
        profileImage.addGestureRecognizer(imagePickerGesture)
        addSwipeLeft()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        convTableView.setContentOffset(CGPoint(x:0, y:self.convTableView.contentOffset.y - (self.refreshControl.frame.size.height)), animated: true)
        refreshControl.beginRefreshing()
        userEmail.text = Auth.auth().currentUser?.email
        DataService.instance.getProfileImage(forUser: (
            Auth.auth().currentUser?.uid)!) { (returnedImage) in
            if let image = returnedImage {
                self.profileImage.image = image
            }
        }
       refreshData()
        
    }

    @objc func refreshData(){
        DataService.instance.getAllConvKeys(forUser: (Auth.auth().currentUser?.uid)!) { (returnedKeys) in
            DataService.instance.getConvsInfo(forKey: returnedKeys, andUser: (Auth.auth().currentUser?.uid)!) {(infos) in
                self.conversations = infos
                self.convTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    @objc func pickUserImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismissFromRight()
    }
    
    @IBAction func signOutButtonTapped(_ sender: Any) {
        AuthService.instance.logOutUser()
        present(storyboard!.instantiateViewController(withIdentifier: "LoginVC"), animated: false, completion: nil)
    }
    
    @objc func showChat(sender : UITapGestureRecognizer){
        //Retrieve the tapped cell
        let tapLocation = sender.location(in: self.convTableView)
        let indexPath : NSIndexPath = self.convTableView.indexPathForRow(at: tapLocation)! as NSIndexPath
        let conv = self.conversations[indexPath.row]
        //Add create conv
        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
        chatVC?.setUpView(forToID: conv["toID"]!, withConvID: conv["convID"]!)
        self.presentFromRight(chatVC!)
    }
}


//MARK:- Image Picker
extension ProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage : UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImage = editedImage
        }else if let original = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = original
        }
        if let selectedImageUser = selectedImage {
            //Upload it to firebase
            profileImage.image = selectedImageUser
            self.userImage = selectedImageUser
            DataService.instance.uploadProfilePicture(forUser: (Auth.auth().currentUser?.uid)!, withProfileImage: selectedImageUser, completion: { (success) in
                //Handle error
            })

        }
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Gestures
extension ProfileVC : UIGestureRecognizerDelegate {
    
    func addSwipeLeft(){
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(closeButtonTapped(_:)))
        swipe.direction = .left
        view.addGestureRecognizer(swipe)
    }
}

extension ProfileVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell") as? ConversationCell else {return UITableViewCell()}
        cell.configureCell(forUser: conversations[indexPath.row]["toID"]!, withTitle: conversations[indexPath.row]["title"]!, andConvID: conversations[indexPath.row]["convID"]!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(showChat(sender:)))
        cell.addGestureRecognizer(tap)
        cell.sizeToFit()
        return cell
    }
    
    
}
