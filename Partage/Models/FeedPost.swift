//
//  FeedPost.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 25/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation

struct FeedPost {
    
    //Private variables
    private var _title : String
    private var _description : String
    private var _senderId : String
    private var _latitude : Double
    private var _longitude : Double
    private var _postedAt : String
    
    //Getters
    var title : String  {
       return _title
    }
    var description : String  {
        return _description
    }
    var senderId : String {
        return _senderId
    }
    var latitude : Double {
        return _latitude
    }
    var longitude : Double {
        return _longitude
    }
    var postedAt : String {
        return _postedAt
    }
    
    //Functions
    init(title : String, description : String, senderId : String, latitude : Double, longitude : Double, postedAt : String) {
        _title = title
        _description = description
        _senderId = senderId
        _latitude = latitude
        _longitude = longitude
        _postedAt = postedAt
    }
    
}
