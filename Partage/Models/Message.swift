//
//  Message.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 02/12/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation


struct Message {
    
    
    private var _toID : String
    private var _fromID : String
    private var _content : String
    private var _postedAt : String
    
    var toID : String {
        return _toID
    }
    var fromID : String {
        return _fromID
    }
    var content : String {
        return _content
    }
    var postedAt : String {
        return _postedAt
    }
    
    init(toID : String, fromID : String, content : String, postedAt : String) {
        _toID = toID
        _fromID = fromID
        _content = content
        _postedAt = postedAt
    }
}
