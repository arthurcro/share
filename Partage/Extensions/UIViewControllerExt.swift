//
//  UIViewControllerExt.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 27/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /*
     Present an app Error
     */
    func presentError(_ error: Error) {
        if let appError = error as? AppError {
            let title = appError.title
            let message = appError.description
            let alertController = UIAlertController(title: title,
                                                    message: message,
                                                    preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "OK",
                                              style: .default,
                                              handler: nil)
            alertController.addAction(dismissAction)
            present(alertController, animated: true)
        } else {
            print(error)
        }
    }
    
    func presentFromRight(_ viewControllerToPresent : UIViewController){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    func dismissFromLeft(){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }
    func presentFromLeft(_ viewControllerToPresent : UIViewController){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    func dismissFromRight(){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }
}

