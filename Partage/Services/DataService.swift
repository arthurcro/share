//
//  DataService.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 24/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseStorage

let DB_BASE = Database.database().reference()

class DataService {
    
    //REFS
    private var  _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.child("users")
    private var _REF_FEED = DB_BASE.child("feed")
    private var _REF_CONV = DB_BASE.child("conversations")
    private var _REF_STORAGE = Storage.storage().reference()
    private(set) var imageCache = NSCache<NSString, UIImage>()
    
    
    //Variables
    var REF_BASE : DatabaseReference {
        return _REF_BASE
    }
    var REF_USERS : DatabaseReference {
        return _REF_USERS
    }
    var REF_STORAGE : StorageReference {
        return _REF_STORAGE
    }
    var REF_FEED  : DatabaseReference {
        return _REF_FEED
    }
    var REF_CONV  : DatabaseReference {
        return _REF_CONV
    }
    
    //Instance
    static let instance = DataService()
    
    
    //Functions
    
    /*
     Add a user to firebase
     */
    func createDBUser(forUID uid : String, withUserData userData : Dictionary<String,Any>){
        REF_USERS.child(uid).updateChildValues(userData)
    }
    
    /*
     Upload a picture to storage
     */
    func uploadProfilePicture(forUser uid : String, withProfileImage image : UIImage, completion : @escaping (_ success : Bool )->()){
        if let uploadImage = image.jpeg(.lowest){
    
            REF_STORAGE.child("images/profilePicture\(uid).png").putData(uploadImage, metadata: nil, completion: { (metadata, error) in
                //Image has been uploaded
                if (error == nil){
                    if  let downloadUrl = metadata?.downloadURL()?.absoluteString {
                        self.REF_USERS.child(uid).updateChildValues(["profilePicture" : downloadUrl])
                        completion(true)
                    }else{
                        return
                    }
                }else{
                    return
                }
            })
        }
    }
    /*
     Get email from uid
     */
    func getEmail(fromSenderId uid : String, completion : @escaping (_ email : String )->()){
        REF_USERS.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let users = dataSnapshot.children.allObjects as? [DataSnapshot] else {
                return
            }
            for user in users {
                if user.key == uid {
                    completion((user.childSnapshot(forPath: "email").value as? String)!)
                }
            }
        }
    }
    /*
     Get profile Image for user
     */
    func getProfileImage(forUser uid : String, completion : @escaping (_ image : UIImage? )->()){
        REF_USERS.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let users = dataSnapshot.children.allObjects as? [DataSnapshot] else {
                completion (nil)
                return
            }
            //For all user we find the one we're interested in
            for user in users {
                if user.key == uid {
                    //We found the user, if they don't have an image we complete with nil
                    guard let imageUrl = user.childSnapshot(forPath: "profilePicture").value as? String else {
                        completion(nil)
                        return
                    }
                    //If the image is already in the cache we don't need to download it again
                    if let cachedImage = self.imageCache.object(forKey: imageUrl as NSString) {
                        completion(cachedImage)
                    }else{
                        //Otherwise we download it and put it in the cahce
                        let imageRef = Storage.storage().reference(forURL: imageUrl)
                        imageRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
                            if error == nil && data != nil {
                                let image = UIImage(data: data!)
                                self.imageCache.setObject(image!, forKey: imageUrl as NSString)
                                completion(image!)
                            }else{
                                completion(nil)
                                return
                            }
                        }
                    }
                }
            }
        }
    }
    
    /*
     Post a feed post
     */
    func postFeed(forUser uid : String, withContent description : String, andTitle title : String, atLatitude latitude : Double, andAtLongitude longitude : Double, completion : @escaping (_ success : Bool )->()){
        //Get date of the post (formated in french here)
        let now = Date()
        let french       = DateFormatter()
        french.dateStyle = .medium
        french.timeStyle = .medium
        french.locale    = Locale(identifier: "FR-fr")
        let postedAt =  french.string(from: now)
        
        //Add the post
        REF_FEED.childByAutoId().updateChildValues(["title" : title, "description" : description, "senderId" : uid, "latitude" : latitude, "longitude" : longitude, "postedAt" : postedAt])
        completion(true)
    }
    
    /*
     Get feed posts
     */
    func getPostFromFeed(completion : @escaping ( _ feedPosts : [FeedPost])->()){
        var feedPosts : [FeedPost] = [FeedPost]()
        REF_FEED.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let returnedfeedPosts = dataSnapshot.children.allObjects as? [DataSnapshot] else {
                completion(feedPosts)
                return
            }
            for returnedPost in returnedfeedPosts {
                
                let senderId = returnedPost.childSnapshot(forPath: "senderId").value as? String
                let title = returnedPost.childSnapshot(forPath: "title").value as? String
                let message = returnedPost.childSnapshot(forPath: "description").value as? String
                let latitude = returnedPost.childSnapshot(forPath: "latitude").value as? Double
                let longitude = returnedPost.childSnapshot(forPath: "longitude").value as? Double
                let postedAt = returnedPost.childSnapshot(forPath: "postedAt").value as? String
                let feedPost = FeedPost(title: title!, description: message!, senderId: senderId!, latitude: latitude!, longitude: longitude!, postedAt : postedAt!)
                feedPosts.append(feedPost)
            }
            completion(feedPosts)
        }
        
    }
    
    /*
     Create new conversation for a post
     */
    func createNewConversation(forPost title : String, withUser1 uid1 : String, andUser2 uid2 : String, completion : @escaping (_ success : Bool, _ key : String)->()){
        //Create new conv and save key
        let key =  REF_CONV.childByAutoId().key
        REF_CONV.child(key).updateChildValues(["toID" : uid1, "fromID" : uid2,"title" : title])
        //Add ref to both users
        REF_USERS.child(uid1).child("conversations").updateChildValues([key : 1])
        REF_USERS.child(uid2).child("conversations").updateChildValues([key : 1])
        completion(true, key)
    }
    /*
     Send message
     */
    func uploadMessage(toConv convID : String, fromUser fromUid : String, toUser toUid : String, withContent content : String, atTime postedAt : String, completion : @escaping (_ success : Bool)->()){
        REF_CONV.child(convID).child("messages").childByAutoId().updateChildValues(["toUid" : toUid, "fromUid" : fromUid, "content" : content, "postedAt" : postedAt])
        completion(true)
    }
    
    /*
     Get all messages from conversation
     */
    func getAllMessages(forConv convid : String, completion : @escaping (_ messages : [Message])->()){
        REF_CONV.child(convid).child("messages").observeSingleEvent(of: .value) { (dataSnapshot) in
            var messages = [Message]()
            guard let returnedMessage = dataSnapshot.children.allObjects as? [DataSnapshot] else{
                return
            }
            for message in returnedMessage {
                
                let content = message.childSnapshot(forPath: "content").value as! String
                let toUid = message.childSnapshot(forPath: "toUid" ).value as! String
                let fromUid = message.childSnapshot(forPath: "fromUid").value as! String
                let postedAt = message.childSnapshot(forPath: "postedAt").value as! String
                
                let newMessage = Message(toID: toUid, fromID: fromUid, content: content, postedAt: postedAt)
                messages.append(newMessage)
            }
            completion(messages) 
        }
    }
    
    /*
     Get all conversation keys for user
     */
    func getAllConvKeys(forUser uid : String, completion : @escaping (_ conversations : [String])->()){
        REF_USERS.child(uid).child("conversations").observeSingleEvent(of: .value) { (dataSnapshot) in
            var conversationKeys : [String] = [String]()
            guard let returnedConvs = dataSnapshot.children.allObjects as? [DataSnapshot] else {
                return
            }
            for returnedConversation in returnedConvs {
                conversationKeys.append(returnedConversation.key)
            }
            completion(conversationKeys)
        }
    }
    
    /*
     Get all conv from keys
     */
    func getConvsInfo(forKey keys : [String],andUser uid : String, completion : @escaping (_ infos : [Dictionary<String, String>])->()){
        var conversations = [Dictionary<String, String>]()
        REF_CONV.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let returnedConvs = dataSnapshot.children.allObjects as? [DataSnapshot] else {return}
            for conversation in returnedConvs {
                let user1 = conversation.childSnapshot(forPath: "fromID").value as! String
                let user2 = conversation.childSnapshot(forPath: "toID").value as! String
                let title = conversation.childSnapshot(forPath: "title").value as! String
                if user1 == uid || user2 == uid {
                    var toID = ""
                    if user1 == uid {
                        toID = user2
                    }else{
                        toID = user1
                    }
                    let conv = ["convID" : conversation.key, "toID" : toID, "title" : title]
                    conversations.append(conv)
                }
            }
            completion(conversations)
        }
    }
    
}
