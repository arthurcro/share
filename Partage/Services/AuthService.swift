//
//  AuthService.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 24/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation
import Firebase

class AuthService {

    static let instance = AuthService()
    
    /*
     Function to register a user with firebase
     */
    func registerUser(forEmail email : String, withPassword password : String, andProfileImage image : UIImage?, completion : @escaping (_ success : Bool, _ errror : Error? )->()){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            guard let user = user else {
                //User not created we have an error
                completion(false, error)
                return
            }
            
            //We have an user, we upload the profile picture
            if let profileImage = image {
                //Upload the picture to storage and give the returned url to the profilePicture user's param
                DataService.instance.uploadProfilePicture(forUser: user.uid, withProfileImage: profileImage, completion: { (returnedUrlImage) in
                    let userData : Dictionary<String,Any> = ["provider": user.providerID, "email" : user.email!, "profileImage" : returnedUrlImage]
                    DataService.instance.createDBUser(forUID: user.uid, withUserData: userData)
                    //Completion true, we quit the function
                    completion(true,nil)
                    return
                })
            }
            
            //No image or we couldn't upload it
            let userData : Dictionary<String,Any> = ["provider": user.providerID, "email" : user.email!]
            DataService.instance.createDBUser(forUID: user.uid, withUserData: userData)
            completion(true,nil)
        }
    }


    /*
     Function to log a user with Firebase
     */
    func logInUser(forEmail email : String, withPassword password : String, completion : @escaping (_ success : Bool, _ error : Error?)->()){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error == nil {
                completion(true,nil)
            }else{
                completion(false,error)
                return
            }
        }
    }
    
    /*
     Function to log out a user
     */
    func logOutUser(){
        do {
            try Auth.auth().signOut()
        }catch {
            print(error.localizedDescription)
        }
    }

}
