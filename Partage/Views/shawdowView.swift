//
//  shawdowView.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 25/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class shawdowView: UIView {
    
    override func awakeFromNib() {
        setUpView()
        super.awakeFromNib()
    }
    
    func setUpView(){
        self.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 10.0
        self.layer.shadowOffset = CGSize(width: 10.0, height: 10.0)
    }
    
}
