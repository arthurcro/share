//
//  ConversationCell.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 03/12/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {
    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var title: UILabel!
    
    var convID : String = ""
    
    func configureCell(forUser uid : String, withTitle title : String, andConvID convID : String){
        DataService.instance.getEmail(fromSenderId: uid) { (email) in
            self.userEmail.text = email
        }
        DataService.instance.getProfileImage(forUser: uid) { (returnedImage) in
            if let image = returnedImage {
                self.userImage.image = image
            }
        }
        self.title.text = title
        self.convID = convID
    }
}
