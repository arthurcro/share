//
//  RoundedImageView.swift
//  BreakPoint
//
//  Created by Arthur Crocquevieille on 19/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {

    
    func setUpView(){
        let cornerRadius : CGFloat =  self.layer.bounds.size.width / 2;
        self.layer.cornerRadius = cornerRadius
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setUpView()
    }
    override func awakeFromNib() {
        self.setUpView()
    }
}
