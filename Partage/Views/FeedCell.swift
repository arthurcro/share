//
//  FeedCell.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 25/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell{

    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var postedAt: UILabel!
    
    
    func configureCell(forUser uid : String , withTitle title : String, andDescription description : String, postedAt : String ){
        titleLabel.text = title
        descriptionLabel.text = description
        self.postedAt.text = postedAt
        DataService.instance.getProfileImage(forUser: uid) { (returnedImage) in
            if returnedImage != nil {
                self.userImage.image = returnedImage!
            }
        }
        DataService.instance.getEmail(fromSenderId: uid) { (email) in
            self.userEmail.text = email
        }
    }
}

