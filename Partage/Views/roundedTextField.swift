//
//  roundedTextField.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 24/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class roundedButton :UIButton {

    func roundCorner(){
        self.layer.cornerRadius = 5.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6009521484)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCorner()
    }
}
