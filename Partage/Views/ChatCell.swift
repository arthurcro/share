//
//  ChatCell.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 03/12/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var textViewLeading: NSLayoutConstraint!
    
    func configureCell(forMessage message : Message){
        textView.text = message.content
        self.textView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        bubbleView.layer.cornerRadius = 15
    }
}
