//
//  MessageError.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 02/12/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation


enum MessageError : AppError {
    case convDidNotCreate
    var title: String {
        return "Erreur"
    }
    var description: String {
        switch self {
        case .convDidNotCreate:
            return "The conversation couldn't be created"
        }
    }
}
