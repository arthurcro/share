//
//  AuthError.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 27/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation


enum AuthError : AppError {
    
    case invalidLogin
    case invalidSignUp
    
    var title: String {
        return "Erreur"
    }
    var description: String {
        switch self {
    
        case .invalidLogin:
            return "Mauvais mot de passe ou email"
        case .invalidSignUp:
            return "Vérifier tous les champs : email, mot de passe et image de profile"
        }
    }
}
