//
//  LocationError.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 27/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation


enum LocationError : AppError {
    case noPermission
    var title: String {
        return "Erreur"
    }
    var description: String {
        switch self {
        case .noPermission:
            return "You need to enable access to your location to be able to post"
        }
    }
}
