
//
//  AppError.swift
//  Partage
//
//  Created by Arthur Crocquevieille on 27/11/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation


protocol AppError: Error {
    var title: String { get }
    var description: String { get }
}
